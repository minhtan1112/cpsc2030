CREATE DATABASE mecha_list;
USE mecha_list;

CREATE TABLE mecha (
  classification text,
  model_number text,
  official_name text,
  head_height text,
  base_weight text,
  full_weight text,
  power_source text,
  generator_output text,
  armor_material text,
  total_thrust text,
  maximum_acceleration text,
  maximum_speed text,
  effective_sensor_radius text,
  crew text,
  image text
);

Insert into mecha values ("Prototype Close Quarters Combat Mobile Suit","RX-78-2","Gundam","18.0 m","43.4 t","60.0 t","Minovsky Ultracompact Fusion Reactor","1380 kW","Luna Titanium","2 × 24000 kg 4 × 1870 kg","0.93 G","165 km/h","5700 m","Pilot only","RX-78-2_Gundam.jpg");
Insert into mecha values ("Prototype General-Purpose Mobile Suit","RX-178","Gundam Mk-II","18.5 m","33.4 t","54.1 t","Minovsky Ultracompact Fusion Reactor","1930 kW","Titanium Alloy Ceramic Composite","4 x 20300 kg","1.50 G","","11300 m","Pilot only","RX-178_-_Gundam_Mk-II.jpg");
Insert into mecha values ("Prototype Newtype-use Transformable Attack-Use Mobile Suit","MSZ-006 MSZ-006-1","Zeta Gundam","19.85 m","28.7 t","62.3 t","Minovsky Ultracompact Fusion Reactor","2020 kW","Gundarium γ Alloy","5 x 12200 kg","1.81 G","","14000 m","Pilot only","MSZ-006_-_Zeta_Gundam.jpg");
Insert into mecha values ("Prototype Newtype-use Transformable Multipurpose Mobile Suit","MSZ-010","ΖΖ Gundam","19.86 m","32.7 t","68.4 t","Minovsky Ultracompact Fusion Reactor","7340 kW","Gundarium γ Alloy","101000 kg","1.48 G","","16200 m","Pilot Additional canopy-style standard cockpit","Msz-010.jpg");
Insert into mecha values ("Limited Production Transformable General-Purpose Mobile Suit","LM312V04","Victory Gundam","15.2 m","7.6 t","17.7 t","Minovsky Ultracompact Fusion Reactor","4780 kW","Gundarium Alloy Super-Ceramic Composite","79700 kg","4.50 G","","","Pilot only","V_Gundam.png");
Insert into mecha values ("Prototype Transformable General-Purpose Mobile Suit","LM314V21","Victory 2 Gundam","15.5 m","11.5 t","15.9 t","Minovsky Ultracompact Fusion Reactor","7510 kW","Gundarium Alloy Super-Ceramic Composite","","20.0 G","","","Pilot only","LM314V21_Victory_2_Gundam.jpg");
