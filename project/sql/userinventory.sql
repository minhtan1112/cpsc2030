CREATE DATABASE inventory;
USE inventory;

CREATE TABLE userInventory (
  official_name text,
  user text
);

Insert into userInventory values ("Gundam","user1");
Insert into userInventory values ("Gundam Mk-II","user1");
Insert into userInventory values ("Zeta Gundam","user1");
Insert into userInventory values ("ΖΖ Gundam","user2");
Insert into userInventory values ("Victory Gundam","user2");
Insert into userInventory values ("Victory 2 Gundam","user3");
