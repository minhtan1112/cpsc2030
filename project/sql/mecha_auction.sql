CREATE DATABASE auction;
USE auction;

CREATE TABLE mechaAuction (
  model_number text,
  official_name text,
  bid text,
  user text
);

Insert into mechaAuction values ("RX-78-2","Gundam","$0","user1");
Insert into mechaAuction values ("RX-178","Gundam Mk-II","$0","user2");
Insert into mechaAuction values ("MSZ-006 MSZ-006-1","Zeta Gundam","$0","user3");
Insert into mechaAuction values ("MSZ-010","ΖΖ Gundam","$0","user4");
Insert into mechaAuction values ("LM312V04","Victory Gundam","$0","user5");
Insert into mechaAuction values ("LM314V21","Victory 2 Gundam","$0","user6");
