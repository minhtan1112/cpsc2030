<?php
include('usersession.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Administrator Site</title>
  <meta charset="utf-8">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <style>
  #main {
    height: 800px;
    width: 1000px;
  }
  h1 {
    text-align: center;
  }
  #userIndex {
    height: 650px;
    width: 150px;
    border: 2px solid black;
    margin-left: 0px;
    text-align: center;
    display: inline-block;
    overflow: auto;
  }
  #auctionIndex {
    height: 650px;
    width: 250px;
    border: 2px solid black;
    margin-left: 50px;
    text-align: center;
    display: inline-block;
    overflow: auto;
  }
  /* #aucIndex {
  height: 650px;
  width: 150px;
  border: 2px solid black;
  margin-left: 0px;
  text-align: center;
  display: inline-block;
  overflow: auto;
  } */
  /* #bidIndex {
  height: 650px;
  width: 150px;
  border: 2px solid black;
  margin-left: 500px;
  text-align: center;
  display: inline-block;
  overflow: auto;
}
#mechaIndex {
height: 650px;
width: 150px;
border: 2px solid black;
margin-left: 0px;
text-align: center;
display: inline-block;
overflow: auto;
} */
#next, #prev {
  margin-top: auto;
}
.detail {
  height: 650px;
  width: 350px;
  border: 2px solid black;
  margin-left: 50px;
  display: inline-block;
  position: absolute;
  overflow: auto;
}
ul {
  margin-right: 40px;
}
li:hover {
  background-color: gray;
}
h2:hover {
  background-color: gray;
}
#modelnumber {
  margin-top: 5px;
  display: block;
  width: 25%;
  border: 2px solid #ccc;
  border-radius: 5px;
  font-size: 15px;
  background-color: white;
  padding: 5px;
  font-family: 'Lora', serif;
}
#officialname {
  margin-top: 5px;
  display: block;
  width: 25%;
  border: 2px solid #ccc;
  border-radius: 5px;
  font-size: 15px;
  background-color: white;
  padding: 5px;
  font-family: 'Lora', serif;
}
#bid {
  margin-top: 5px;
  display: block;
  width: 25%;
  border: 2px solid #ccc;
  border-radius: 5px;
  font-size: 15px;
  background-color: white;
  padding: 5px;
  font-family: 'Lora', serif;
}
#mySubmit {
  margin-top: 5px;
  margin-left: 110px;
  width: auto;
  padding: 10px;
  border-radius: 5px;
  color: white;
  background-color: #2D7F3E;
  font-size: 20px;
  text-align: center;
  font-family: 'Lora', serif;
}
#mySubmit:hover {
  background-color: #667F6B;
}
</style>
</head>
<body>
  <div id="main">
    <h1>User Site</h1>
    <h1>Welcome <label style="color: red;"><?php echo $login_session; ?></label><a style="text-decoration: none;" href = "logout.php"> Sign Out</a></h1>
    <div id="userIndex">
      <h2>My inventory</h2>
      <ul><label id="inventory"></label></ul>
    </div>
    <div id="auctionIndex">
      <h2>Current auctions</h2>
      <ul><label id="currentAuction"></label></ul>
    </div>
    <form id="contactForm" method="post" name="myForm">
      <label for="modelnumber">Model number:</label>
      <input type="text" name="modelnumber" id="modelnumber">
      <label for="officialname">Official name:</label>
      <input type="text" name="officialname" id="officialname">
      <label for="bid">Bid:</label>
      <input type="number" name="bid" id="bid">
      <input id="mySubmit" type="submit" value="Create auction">
    </form>
  </div>
  <?php
  // Initialize variable for database credentials
  require_once("sql/sqlhelperUserInventory.php");
  require_once("sql/sqlhelperAuction.php");
  //Check connection was successful
  if ($db_inventory->connect_errno) {
    printf("Failed to connect to database");
    exit();
  }
  if ($db_auction->connect_errno) {
    printf("Failed to connect to database");
    exit();
  }

  //Fetch 3 rows from actor table
  $result1 = $db_inventory->query("SELECT official_name FROM userInventory WHERE user = '$login_session'");
  $result2 = $db_auction->query("SELECT model_number, official_name, bid FROM mechaAuction");

  //Initialize array variable
  $dbInventory = array();
  $dbAuction = array();

  //Fetch into associative array
  while ( $row = $result1->fetch_assoc())  {
    $dbInventory[]=$row;
  }
  while ( $row = $result2->fetch_assoc())  {
    $dbAuction[]=$row;
  }
  ?>
  <script>
    var objInventory = <?php echo json_encode($dbInventory);?>;
    let stringInventory = "";
    let elementInv = document.querySelector("#inventory");
    for (var i = 0; i < objInventory.length; i++) {
      stringInventory += objInventory[i].official_name + "<br>";
    }
    elementInv.innerHTML = stringInventory;

    var objAuction = <?php echo json_encode($dbAuction);?>;
    let stringAuction = "";
    let elementAuc = document.querySelector("#currentAuction");
    for (var i = 0; i < objAuction.length; i++) {
      stringAuction += "<li>" + objAuction[i].model_number + " " + objAuction[i].official_name + " " + objAuction[i].bid + "</li>"
      + "<input id='' type='submit' value='Submit a bid'>";
    }
    elementAuc.innerHTML = stringAuction;
  </script>
</body>
</html>
