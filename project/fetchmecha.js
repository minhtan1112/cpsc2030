let URLMecha = `listofmecha.php`;
console.log(URLMecha);
let requestMecha = new Request(URLMecha, {
  method: "GET",
  headers: {
    'Content-Type': 'application/json'
  },
  redirect: "follow"
});

fetch(requestMecha).then(function (response) {
  if (!response.ok) {
    return Promise.reject("Network error");
  }
  return response.json();
}).then(function (value) {
  if (value.length === 0) {
    return Promise.reject("Not found");
  } else {
    let obj = value;
    let htmlString = "";
    let element = document.querySelector("#mecha");
    for (let i = 0; i < obj.length; i++) {
      htmlString += "<li onclick='checkMecha(this.id)' id = '" + obj[i].official_name
      + "'>" + obj[i].official_name + "</li>";
    }
    element.innerHTML = htmlString;
  }
}).catch(function (rejection) {
  console.log(rejection);
});

function checkMecha(id) {
  $.ajax({
    url: 'mechadetail.php?id=' + id,
    success: function (value) {
      let obj = JSON.parse(value);
      let htmlString = "";
      let element = document.querySelector("#mechaDetail");
      htmlString = "classification: " + obj[0].classification + "<br>model_number: " + obj[0].model_number
      + "<br>official_name: " + obj[0].official_name
      + "<br>head_height: " + obj[0].head_height + "<br>base_weight: "
      + obj[0].base_weight + "<br>full_weight: " + obj[0].full_weight
      + "<br>power_source: " + obj[0].power_source + "<br>generator_output: " + obj[0].generator_output
      + "<br>armor_material: " + obj[0].armor_material
      + "<br>total_thrust: " + obj[0].total_thrust + "<br>maximum_acceleration: "
      + obj[0].maximum_acceleration + "<br>maximum_speed: " + obj[0].maximum_speed
      + "<br>effective_sensor_radius: "
      + obj[0].effective_sensor_radius + "<br>crew: " + obj[0].crew + "<br>"
      + "<img height='250' width='200' src='images/" + obj[0].image + "' alt='Error'>";
      element.innerHTML = htmlString;
    }
  });
  return false;
}
