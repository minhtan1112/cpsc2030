let URLUser = `listofusers.php`;
console.log(URLUser);
let requestUser = new Request(URLUser, {
  method: "GET",
  headers: {
    'Content-Type': 'application/json'
  },
  redirect: "follow"
});

fetch(requestUser).then(function (response) {
  if (!response.ok) {
    return Promise.reject("Network error");
  }
  return response.json();
}).then(function (value) {
  if (value.length === 0) {
    return Promise.reject("Not found");
  } else {
    let obj = value;
    let htmlString = "";
    let element = document.querySelector("#user");
    for (let i = 0; i < obj.length; i++) {
      htmlString += "<li onclick='checkUser(this.id)' id = '" + obj[i].username
      + "'>" + obj[i].username + "</li>";
    }
    element.innerHTML = htmlString;
  }
}).catch(function (rejection) {
  console.log(rejection);
});

function checkUser(id) {
  $.ajax({
    url: 'userinventory.php?id=' + id,
    success: function (value) {
      let obj = JSON.parse(value);
      let htmlString = "";
      let element = document.querySelector("#userInventory");
      for (var i = 0; i < obj.length; i++) {
        htmlString += obj[i].official_name + "<br>";
      }
      element.innerHTML = htmlString;
    }
  });
  return false;
}
