<?php
// Initialize variable for database credentials
require_once("sql/sqlhelperMechaDetail.php");

//Check connection was successful
if ($db_mechadetail->connect_errno) {
  printf("Failed to connect to database");
  exit();
}

$id = $_GET['id'];

//Fetch 3 rows from actor table
$result = $db_mechadetail->query("SELECT * FROM mecha WHERE official_name = '$id'");

//Initialize array variable
$dbdata = array();

//Fetch into associative array
while ( $row = $result->fetch_assoc())  {
  $dbdata[]=$row;
}

//Print array in JSON format
echo json_encode($dbdata);
?>
