<?php
// Initialize variable for database credentials
require_once("sql/sqlhelperMecha.php");

//Check connection was successful
if ($db_mecha->connect_errno) {
  printf("Failed to connect to database");
  exit();
}

//Fetch 3 rows from actor table
$result = $db_mecha->query("SELECT official_name FROM mecha");

//Initialize array variable
$dbdata = array();

//Fetch into associative array
while ( $row = $result->fetch_assoc())  {
  $dbdata[]=$row;
}

//Print array in JSON format
echo json_encode($dbdata);
?>
