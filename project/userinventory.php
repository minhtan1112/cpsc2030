<?php
// Initialize variable for database credentials
require_once("sql/sqlhelperUserInventory.php");

//Check connection was successful
if ($db_inventory->connect_errno) {
  printf("Failed to connect to database");
  exit();
}

$id = $_GET['id'];

//Fetch 3 rows from actor table
$result = $db_inventory->query("SELECT official_name FROM userInventory WHERE user = '$id'");

//Initialize array variable
$dbdata = array();

//Fetch into associative array
while ( $row = $result->fetch_assoc())  {
  $dbdata[]=$row;
}

//Print array in JSON format
echo json_encode($dbdata);
?>
