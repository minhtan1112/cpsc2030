CREATE DATABASE Pokedex;
USE Pokedex;

CREATE TABLE pokemon (
    national int,
    name text,
    type text,
    image text
);

CREATE TABLE attributes (
    name text,
    type text,
    hp int,
    atk int,
    def int,
    sat int,
    sdf int,
    spd int,
    bst int
);

CREATE TABLE Strong_Against (
    type text,
    Strong_Against text
);

CREATE TABLE Weak_Against (
    type text,
    Weak_Against text
);

CREATE TABLE Resistant_To (
    type text,
    Resistant_To text
);

CREATE TABLE Vulnerable_To (
    type text,
    Vulnerable_To text
);

Insert into pokemon values (1,"Bulbasaur","Grass","https://www.azurilland.com/pokedex/1-bulbasaur");
Insert into pokemon values (1,"Bulbasaur","Poison","https://www.azurilland.com/pokedex/1-bulbasaur");
Insert into pokemon values (2,"Ivysaur","Grass","https://www.azurilland.com/pokedex/2-ivysaur");
Insert into pokemon values (2,"Ivysaur","Poison","https://www.azurilland.com/pokedex/2-ivysaur");
Insert into pokemon values (3,"Venusaur","Grass","https://www.azurilland.com/pokedex/3-venusaur");
Insert into pokemon values (3,"Venusaur","Poison","https://www.azurilland.com/pokedex/3-venusaur");
Insert into pokemon values (3,"Mega Venusaur","Grass","https://www.azurilland.com/pokedex/10033-mega-venusaur");
Insert into pokemon values (3,"Mega Venusaur","Poison","https://www.azurilland.com/pokedex/10033-mega-venusaur");
Insert into pokemon values (4,"Charmander","Fire","https://www.azurilland.com/pokedex/4-charmander");
Insert into pokemon values (5,"Charmeleon","Fire","https://www.azurilland.com/pokedex/5-charmeleon");
Insert into pokemon values (6,"Charizard","Fire","https://www.azurilland.com/pokedex/6-charizard");
Insert into pokemon values (6,"Charizard","Flying","https://www.azurilland.com/pokedex/6-charizard");
Insert into pokemon values (6,"Mega Charizard X","Fire","https://www.azurilland.com/pokedex/100072-mega-charizard-x");
Insert into pokemon values (6,"Mega Charizard X","Dragon","https://www.azurilland.com/pokedex/100072-mega-charizard-x");
Insert into pokemon values (6,"Mega Charizard Y","Fire","https://www.azurilland.com/pokedex/100073-mega-charizard-y");
Insert into pokemon values (6,"Mega Charizard Y","Flying","https://www.azurilland.com/pokedex/100073-mega-charizard-y");
Insert into pokemon values (7,"Squirtle","Water","https://www.azurilland.com/pokedex/7-squirtle");
Insert into pokemon values (8,"Wartortle","Water","https://www.azurilland.com/pokedex/8-wartortle");
Insert into pokemon values (9,"Blastoise","Water","https://www.azurilland.com/pokedex/9-blastoise");
Insert into pokemon values (9,"Mega Blastoise","Water","https://www.azurilland.com/pokedex/100074-mega-blastoise");
Insert into pokemon values (10,"Caterpie","Bug","https://www.azurilland.com/pokedex/10-caterpie");
Insert into pokemon values (11,"Metapod","Bug","https://www.azurilland.com/pokedex/11-metapod");
Insert into pokemon values (12,"Butterfree","Bug","https://www.azurilland.com/pokedex/12-butterfree");
Insert into pokemon values (12,"Butterfree","Flying","https://www.azurilland.com/pokedex/12-butterfree");
Insert into pokemon values (13,"Weedle","Bug","https://www.azurilland.com/pokedex/13-weedle");
Insert into pokemon values (13,"Weedle","Poison","https://www.azurilland.com/pokedex/13-weedle");
Insert into pokemon values (14,"Kakuna","Bug","https://www.azurilland.com/pokedex/14-kakuna");
Insert into pokemon values (14,"Kakuna","Poison","https://www.azurilland.com/pokedex/14-kakuna");
Insert into pokemon values (15,"Beedrill","Bug","https://www.azurilland.com/pokedex/15-beedrill");
Insert into pokemon values (15,"Beedrill","Poison","https://www.azurilland.com/pokedex/15-beedrill");
Insert into pokemon values (15,"Mega Beedrill","Bug","https://www.azurilland.com/pokedex/1601-mega-beedrill");
Insert into pokemon values (15,"Mega Beedrill","Poison","https://www.azurilland.com/pokedex/1601-mega-beedrill");
Insert into pokemon values (16,"Pidgey","Normal","https://www.azurilland.com/pokedex/16-pidgey");
Insert into pokemon values (16,"Pidgey","Flying","https://www.azurilland.com/pokedex/16-pidgey");
Insert into pokemon values (17,"Pidgeotto","Normal","https://www.azurilland.com/pokedex/17-pidgeotto");
Insert into pokemon values (17,"Pidgeotto","Flying","https://www.azurilland.com/pokedex/17-pidgeotto");
Insert into pokemon values (18,"Pidgeot","Normal","https://www.azurilland.com/pokedex/18-pidgeot");
Insert into pokemon values (18,"Pidgeot","Flying","https://www.azurilland.com/pokedex/18-pidgeot");
Insert into pokemon values (18,"Mega Pidgeot","Normal","https://www.azurilland.com/pokedex/1901-mega-pidgeot");
Insert into pokemon values (18,"Mega Pidgeot","Flying","https://www.azurilland.com/pokedex/1901-mega-pidgeot");
Insert into pokemon values (19,"Rattata","Normal","https://www.azurilland.com/pokedex/19-rattata");
Insert into pokemon values (20,"Raticate","Normal","https://www.azurilland.com/pokedex/20-raticate");

Insert into attributes values ("Bulbasaur","Grass",45,49,49,65,65,45,318);
Insert into attributes values ("Bulbasaur","Poison",45,49,49,65,65,45,318);
Insert into attributes values ("Ivysaur","Grass",60,62,63,80,80,60,405);
Insert into attributes values ("Ivysaur","Poison",60,62,63,80,80,60,405);
Insert into attributes values ("Mankey","Fighting",40,80,35,35,45,70,305);
Insert into attributes values ("Meowth","Normal",40,45,35,40,40,90,290);
Insert into attributes values ("Magnemite","Electric",25,35,70,95,55,45,325);
Insert into attributes values ("Magnemite","Steel",25,35,70,95,55,45,325);
Insert into attributes values ("Charmander","Fire",39,52,43,60,50,65,309);
Insert into attributes values ("Pikachu","Electric",35,55,40,50,50,90,320);
Insert into attributes values ("Diglett","Ground",10,55,25,35,45,95,265);

Insert into Vulnerable_To values ("Normal","Fighting");
Insert into Vulnerable_To values ("Steel","Fighting");
Insert into Vulnerable_To values ("Steel","Ground");
Insert into Vulnerable_To values ("Steel","Fire");
Insert into Vulnerable_To values ("Dark","Fighting");
Insert into Vulnerable_To values ("Dark","Bug");
Insert into Vulnerable_To values ("Dark","Fairy");

Insert into Strong_Against values ("Steel","Rock");
Insert into Strong_Against values ("Steel","Ice");
Insert into Strong_Against values ("Steel","Fairy");
Insert into Strong_Against values ("Fire","Bug");
Insert into Strong_Against values ("Fire","Steel");
Insert into Strong_Against values ("Fire","Grass");
Insert into Strong_Against values ("Fire","Ice");
Insert into Strong_Against values ("Electric","Flying");
Insert into Strong_Against values ("Electric","Water");

Insert into Weak_Against values ("Ground","Flying");
Insert into Weak_Against values ("Ground","Bug");
Insert into Weak_Against values ("Ground","Grass");
Insert into Weak_Against values ("Electric","Ground");
Insert into Weak_Against values ("Electric","Grass");
Insert into Weak_Against values ("Electric","Electric");
Insert into Weak_Against values ("Electric","Dragon");
