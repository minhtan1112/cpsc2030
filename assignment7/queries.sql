SELECT *
FROM attributes
ORDER BY def DESC LIMIT 10

SELECT attributes.name, attributes.type
FROM attributes
INNER JOIN Vulnerable_To ON attributes.type = Vulnerable_To.type
INNER JOIN Strong_Against ON attributes.type = Strong_Against.type
WHERE Vulnerable_To='Fighting' AND Strong_Against='Ice'

SELECT attributes.name, attributes.type
FROM attributes
INNER JOIN Strong_Against ON attributes.type = Strong_Against.type
INNER JOIN Weak_Against ON attributes.type = Weak_Against.type
WHERE attributes.bst BETWEEN 300 AND 400 AND Strong_Against='Flying' AND Weak_Against='Grass'
