let URLString = `https://pokeapi.co/api/v2/pokemon-species/`;
console.log(URLString);
let request = new Request(URLString, {
  method: "GET",
  headers: {
    'Content-Type': 'application/json'
  },
  redirect: "follow"
});
let nextValue = "";
let prevValue = "";

fetch(request).then(function (response) {
  if (!response.ok) {
    return Promise.reject("Network error");
  }
  return response.json();
}).then(function (value) {
  if (value.length === 0) {
    return Promise.reject("Not found");
  } else {
    let obj = value;
    let htmlString = "";
    let element = document.querySelector("#spec");
    for (let i = 0; i < obj.results.length; i++) {
      htmlString += "<li onclick='checkSpec(this.id)' id = '" + obj.results[i].name
      + "'>" + obj.results[i].name + "</li>";
    }
    element.innerHTML = htmlString;
    nextValue = obj.next;
    prevValue = obj.previous;
    return value;
  }
}).catch(function (rejection) {
  console.log(rejection);
});

function checkSpec(name) {
  let URLString = `https://pokeapi.co/api/v2/pokemon-species/` + name;
  console.log(URLString);
  let request = new Request(URLString, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: "follow"
  });

  fetch(request).then(function (response) {
    if (!response.ok) {
      return Promise.reject("Network error");
    }
    return response.json();
  }).then(function (value) {
    if (value.length === 0) {
      return Promise.reject("Not found");
    } else {
      let htmlString = "";
      let str1, str2 = "";
      let obj = value;
      let specDetail = document.querySelector("#specDetail");
      if (obj.evolves_from_species !== null) {
        str1 = obj.evolves_from_species.name;
      } else {
        str1 = obj.evolves_from_species;
      }
      if (obj.habitat !== null) {
        str2 = obj.habitat.name;
      } else {
        str2 = obj.habitat;
      }
      specString = "id: " + obj.id + "<br>name: " + obj.name + "<br>order: " + obj.order
      + "<br>gender_rate: " + obj.gender_rate + "<br>capture_rate: "
      + obj.capture_rate + "<br>base_happiness: " + obj.base_happiness
      + "<br>is_baby: " + obj.is_baby + "<br>hatch_counter: " + obj.hatch_counter
      + "<br>has_gender_differences: " + obj.has_gender_differences
      + "<br>growth_rate: " + obj.growth_rate.name + "<br>egg_groups: "
      + obj.egg_groups[0].name + "<br>color: " + obj.color.name
      + "<br>shape: " + obj.shape.name + "<br>evolves_from_species: "
      + str1 + "<br>habitat: " + str2 + "<br>generation: " + obj.generation.name
      + "<br>genera: " + obj.genera[0].genus + "<br>language: "
      + obj.genera[0].language.name + "<br>";
      specDetail.innerHTML = specString;
      let characterList = document.querySelector("#characterList");
      let charString = "";
      charString = "<h2 onclick='checkChar(this.id)' id = '" + obj.id + "'>Characteristics(click):</h2>";
      characterList.innerHTML = charString;
      return value;
    }
  }).catch(function (rejection) {
    console.log(rejection);
  });
}

function checkChar(id) {
  let URLString = `https://pokeapi.co/api/v2/characteristic/` + id;
  console.log(URLString);
  let request = new Request(URLString, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: "follow"
  });

  fetch(request).then(function (response) {
    if (!response.ok) {
      let character = document.querySelector("#character");
      character.innerHTML = "Not found";
      return Promise.reject("Network error");
    }
    return response.json();
  }).then(function (value) {
    if (value.length === 0) {
      return Promise.reject("Not found");
    } else {
      let char = "";
      let obj = value;
      let character = document.querySelector("#character");
      for (var i = 0; i < obj.descriptions.length; i++) {
        char += obj.descriptions[i].description + "<br>";
      }
      character.innerHTML = char;
      return value;
    }
  }).catch(function (rejection) {
    console.log(rejection);
  });
}

function getNext() {
  if (nextValue !== null) {
    let URLString = nextValue;
    console.log(URLString);
    let request = new Request(URLString, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: "follow"
    });

    fetch(request).then(function (response) {
      if (!response.ok) {
        return Promise.reject("Network error");
      }
      return response.json();
    }).then(function (value) {
      if (value.length === 0) {
        return Promise.reject("Not found");
      } else {
        let obj = value;
        let htmlString = "";
        let element = document.querySelector("#spec");
        for (let i = 0; i < obj.results.length; i++) {
          htmlString += "<li onclick='checkSpec(this.id)' id = '" + obj.results[i].name
          + "'>" + obj.results[i].name + "</li>";
        }
        element.innerHTML = htmlString;
        nextValue = obj.next;
        prevValue = obj.previous;
        return value;
      }
    }).catch(function (rejection) {
      console.log(rejection);
    });
  }
}

function getPrev() {
  if (prevValue !== null) {
    let URLString = prevValue;
    console.log(URLString);
    let request = new Request(URLString, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: "follow"
    });

    fetch(request).then(function (response) {
      if (!response.ok) {
        return Promise.reject("Network error");
      }
      return response.json();
    }).then(function (value) {
      if (value.length === 0) {
        return Promise.reject("Not found");
      } else {
        let obj = value;
        let htmlString = "";
        let element = document.querySelector("#spec");
        for (let i = 0; i < obj.results.length; i++) {
          htmlString += "<li onclick='checkSpec(this.id)' id = '" + obj.results[i].name
          + "'>" + obj.results[i].name + "</li>";
        }
        element.innerHTML = htmlString;
        nextValue = obj.next;
        prevValue = obj.previous;
        return value;
      }
    }).catch(function (rejection) {
      console.log(rejection);
    });
  }
}
