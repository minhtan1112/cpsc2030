let countF = 0;
let countB = 0;

function stepForward() {
    let n = document.getElementById('n');
    let N = parseInt(n.value);
    let M = 4;
    let r = 0;
    let R1 = document.getElementById('r1');
    let r1 = parseInt(R1.value);
    let R2 = document.getElementById('r2');
    let r2 = parseInt(R2.value);
    let picString = "";
    let i1 = document.getElementById('i');
    let I = parseInt(i1.value);
    let maxN = document.getElementById('maxNum');
    let maxNum = 0;
    if(maxN.value == "M*N") {
      maxNum = M*N;
    } else {
      maxNum = parseInt(maxN.value);
    }
    let x = document.getElementById('x');
    let X = parseInt(x.value);
    let y = document.getElementById('y');
    let Y = parseInt(y.value);
    let output = "";

    if(countF < maxNum) {
        for(let i = I; i <= countF; i += 1) {
            switch(i % M) {
                case 0:
                case 1: r = r1;
                    break;
                case 2:
                case 3: r = r2;
                    break;
            }

            let x = r * Math.cos(i * 2 * Math.PI / (N * M)) + X;
            let y = r * Math.sin(i * 2 * Math.PI / (N * M)) + Y;

            picString += `${x},${y} `;
            output += "<tr><td>" + i + "</td><td>" + r + "</td><td>" + x + "</td><td>"
                      + y + "</td><td>" + picString + "</td></tr>";
            svgString = `<polygon points="${picString}" stroke="red" fill="red"/>`;
            document.querySelector("#output").innerHTML = svgString;
        }
        console.log('The loop has finished executing!');
        document.getElementById('table').innerHTML = "<tr><th>i</th><th>r</th><th>x</th><th>y</th><th>picString</th></tr>"
                                                     + output;
        if(countF != maxNum) {
            countF++;
            countB = countF - 2;
        }
    }
    if(countF == maxNum) {
        countB = countF - 2;
    }
}

function stepBack() {
    let n = document.getElementById('n');
    let N = parseInt(n.value);
    let M = 4;
    let r = 0;
    let R1 = document.getElementById('r1');
    let r1 = parseInt(R1.value);
    let R2 = document.getElementById('r2');
    let r2 = parseInt(R2.value);
    let picString = "";
    let i1 = document.getElementById('i');
    let I = parseInt(i1.value);
    let maxN = document.getElementById('maxNum');
    let maxNum = 0;
    if(maxN.value == "M*N") {
      maxNum = M*N;
    } else {
      maxNum = parseInt(maxN.value);
    }
    let x = document.getElementById('x');
    let X = parseInt(x.value);
    let y = document.getElementById('y');
    let Y = parseInt(y.value);
    let output = "";

    if(countB >= I) {
        console.log('The loop has not begun to execute!');
        for(let i = I; i <= countB; i += 1) {
            switch(i % M) {
                case 0:
                case 1: r = r1;
                    break;
                case 2:
                case 3: r = r2;
                    break;
            }

            let x = r * Math.cos(i * 2 * Math.PI / (N * M)) + X;
            let y = r * Math.sin(i * 2 * Math.PI / (N * M)) + Y;

            picString += `${x},${y} `;
            output += "<tr><td>" + i + "</td><td>" + r + "</td><td>" + x + "</td><td>"
                      + y + "</td><td>" + picString + "</td></tr>";
            svgString = `<polygon points="${picString}" stroke="red" fill="red"/>`;
            document.querySelector("#output").innerHTML = svgString;
        }
        document.getElementById('table').innerHTML = "<tr><th>i</th><th>r</th><th>x</th><th>y</th><th>picString</th></tr>"
                                                     + output;
        if(countB != I) {
            countB--;
            countF = countB + 2;
        } else {
            countB = I;
            countF = countB + 1;
        }
    }
}

function reset() {
    countF = 0;
    countB = 0;
    let picString = "";
    let output = "";
    document.getElementById('table').innerHTML = "<tr><th>i</th><th>r</th><th>x</th><th>y</th><th>picString</th></tr>"
                                                 + output;
    svgString = `<polygon points="${picString}" stroke="red" fill="red"/>`;
    document.querySelector("#output").innerHTML = svgString;
}

let stepF = document.getElementById('stepForward');
stepF.addEventListener('click', stepForward, false);

let stepB = document.getElementById('stepBack');
stepB.addEventListener('click', stepBack, false);

let r = document.getElementById('reset');
r.addEventListener('click', reset, false);
