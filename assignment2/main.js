let URLString = `https://rickandmortyapi.com/api/episode/`;
console.log(URLString);
let request = new Request(URLString);
let eps = [];

fetch(request)
.then(response => response.json())
.then(function (value) {
  if (value.length == 0) {
    return Promise.reject("Not found");
  } else {
    let htmlString = "";
    let obj = value;
    let i = 0;
    let element = document.querySelector("#eps");
    for (var variable in obj.results) {
      htmlString += "<li onclick='checkEps(this.id)' id = '" + variable + "'>" + variable + "</li>";
      eps[i] = variable;
      i++;
    }
    element.innerHTML = htmlString;
    return value;
  }
}).catch(function (rejection) {
  console.log(rejection);
})

function checkEps(id) {
  let eps = id;
  let URLString = `https://rickandmortyapi.com/api/episode/` + id;
  console.log(URLString);
  let request = new Request(URLString);

  fetch(request)
  .then(response => response.json())
  .then(function (value) {
    if (value.length === 0) {
      return Promise.reject("Not found");
    } else {
      let htmlString = "";
      let obj = value;
      let espDetail = document.querySelector("#espDetail");
      epsString = obj.name + "<br>" + obj.air_date + "<br>" + obj.episode + "<br>" + obj.url + "<br>" + obj.created + "<br>";
      espDetail.innerHTML = epsString;
      let characterList = document.querySelector("#characterList");
      let charString = "";
      for (var i = 0; i < obj.characters.length; i++) {
        let string = obj.characters[i].toString();
        string = string.match(/\d+/g);
        charString += "<li onclick='checkChar(this.id)' id = '" + string + "'>" + string + "</li>";
      }
      characterList.innerHTML = charString;
      return value;
    }
  }).catch(function (rejection) {
    console.log(rejection);
  })
}

function checkChar(id) {
  let char = id;
  let URLString = `https://rickandmortyapi.com/api/character/` + id;
  console.log(URLString);
  let request = new Request(URLString);

  fetch(request)
  .then(response => response.json())
  .then(function (value) {
    if (value.length === 0) {
      return Promise.reject("Not found");
    } else {
      let htmlString = "";
      let obj = value;
      let character = document.querySelector("#character");
      let string = obj.image.toString();
      string = string.match(/\d+\.\D+/g);
      char = "<img src='" + obj.image + "' alt='" + string + "'>";
      character.innerHTML = char;
      return value;
    }
  }).catch(function (rejection) {
    console.log(rejection);
  })
}

function getNext() {

}
function getPrev() {

}
