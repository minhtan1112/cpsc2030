<!DOCTYPE html>
<html lang="en">
<head>
  <title>Assignment 5</title>
  <meta charset="utf-8">
  <!-- <script src="main.js" style="text/javascript" defer></script> -->
  <link rel = "stylesheet" type = "text/css" href = "style/style.css"/>
  <style>
  </style>
</head>
<body>
  <div id="main">
    <h1>Item Collection Game</h1>
    <div id="index">
      <?php
      define('up',"up");
      define('down',"down");
      define('left',"left");
      define('right',"right");
      define('reset',"reset");
      define('start',"start");
      define('command',"command");

      session_start();

      if(!array_key_exists("playerPoint_x", $_SESSION)) {
        $_SESSION["playerPoint_x"] = 0;
      }
      if(!array_key_exists("playerPoint_y", $_SESSION)) {
        $_SESSION["playerPoint_y"] = 0;
      }

      if(!isset($_SESSION['gemPoint1_x'], $_SESSION['gemPoint1_y'], $_SESSION['gemPoint2_x'], $_SESSION['gemPoint2_y'], $_SESSION['gemPoint3_x'], $_SESSION['gemPoint3_y'])) {
        $_SESSION['gemPoint1_x'] = rand(0, 9);
        $_SESSION['gemPoint1_y'] = rand(0, 9);
        $_SESSION['gemPoint2_x'] = rand(0, 9);
        $_SESSION['gemPoint2_y'] = rand(0, 9);
        $_SESSION['gemPoint3_x'] = rand(0, 9);
        $_SESSION['gemPoint3_y'] = rand(0, 9);
      }

      if(!array_key_exists("board", $_SESSION)) {
        $_SESSION["board"] = array();
      }
      if(!array_key_exists("gem", $_SESSION)) {
        $_SESSION["gem"] = array();
      }
      if(!array_key_exists("player", $_SESSION)) {
        $_SESSION["player"] = array();
      }
      if(!array_key_exists("consumeGem1", $_SESSION)) {
        $_SESSION["consumeGem1"] = 0;
      }
      if(!array_key_exists("consumeGem2", $_SESSION)) {
        $_SESSION["consumeGem2"] = 0;
      }
      if(!array_key_exists("consumeGem3", $_SESSION)) {
        $_SESSION["consumeGem3"] = 0;
      }

      function startGame() {
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
            $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
            $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
          }
        }
        $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']] = "<img width='60px' height='60px' src='image/Solid_black.svg' alt='black'>";
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($row == $_SESSION['playerPoint_x'] && $col == $_SESSION['playerPoint_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint1_x'] && $col == $_SESSION['gemPoint1_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint2_x'] && $col == $_SESSION['gemPoint2_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint3_x'] && $col == $_SESSION['gemPoint3_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']];
              echo $_SESSION['board'][$row][$col];
            } else {
              echo $_SESSION['board'][$row][$col];
            }
          }
          echo "<br>";
        }
      }
      if(!array_key_exists(command,$_GET)){
        startGame(); //Default option, is to start the game
        //If no command is given
      } else {
        if($_GET[command] == start){
          startGame();
        }
        if($_GET[command] == reset){
          session_destroy();
          session_start();
        }
        if($_GET[command] == down){
          down();
        }
        if($_GET[command] == up){
          up();
        }
        if($_GET[command] == left){
          left();
        }
        if($_GET[command] == right){
          right();
        }
      }
      function down() {
        if ($_SESSION["playerPoint_x"] >= 0 && $_SESSION["playerPoint_x"] < 9) {
          $_SESSION["playerPoint_x"] = $_SESSION["playerPoint_x"] + 1;
        }
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem3"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem2"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem1"] == 0 && $_SESSION["consumeGem2"] == 0 && $_SESSION["consumeGem3"] == 0) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
          }
        }
        $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']] = "<img width='60px' height='60px' src='image/Solid_black.svg' alt='black'>";
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($row == $_SESSION['playerPoint_x'] && $col == $_SESSION['playerPoint_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint1_x'] && $col == $_SESSION['gemPoint1_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint2_x'] && $col == $_SESSION['gemPoint2_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint3_x'] && $col == $_SESSION['gemPoint3_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']];
              echo $_SESSION['board'][$row][$col];
            } else {
              echo $_SESSION['board'][$row][$col];
            }
          }
          echo "<br>";
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint1_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint1_y']) {
          if ($_SESSION["consumeGem1"] == 0) {
            score();
          }
          $_SESSION["consumeGem1"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint2_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint2_y']) {
          if ($_SESSION["consumeGem2"] == 0) {
            score();
          }
          $_SESSION["consumeGem2"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint3_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint3_y']) {
          if ($_SESSION["consumeGem3"] == 0) {
            score();
          }
          $_SESSION["consumeGem3"] = 1;
        }
      }
      function up() {
        if ($_SESSION["playerPoint_x"] > 0 && $_SESSION["playerPoint_x"] <= 9) {
          $_SESSION["playerPoint_x"] = $_SESSION["playerPoint_x"] - 1;
        }
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem3"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem2"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem1"] == 0 && $_SESSION["consumeGem2"] == 0 && $_SESSION["consumeGem3"] == 0) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
          }
        }
        $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']] = "<img width='60px' height='60px' src='image/Solid_black.svg' alt='black'>";
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($row == $_SESSION['playerPoint_x'] && $col == $_SESSION['playerPoint_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint1_x'] && $col == $_SESSION['gemPoint1_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint2_x'] && $col == $_SESSION['gemPoint2_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint3_x'] && $col == $_SESSION['gemPoint3_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']];
              echo $_SESSION['board'][$row][$col];
            } else {
              echo $_SESSION['board'][$row][$col];
            }
          }
          echo "<br>";
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint1_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint1_y']) {
          if ($_SESSION["consumeGem1"] == 0) {
            score();
          }
          $_SESSION["consumeGem1"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint2_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint2_y']) {
          if ($_SESSION["consumeGem2"] == 0) {
            score();
          }
          $_SESSION["consumeGem2"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint3_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint3_y']) {
          if ($_SESSION["consumeGem3"] == 0) {
            score();
          }
          $_SESSION["consumeGem3"] = 1;
        }
      }
      function right() {
        if ($_SESSION["playerPoint_y"] >= 0 && $_SESSION["playerPoint_y"] < 9) {
          $_SESSION["playerPoint_y"] = $_SESSION["playerPoint_y"] + 1;
        }
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem3"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem2"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem1"] == 0 && $_SESSION["consumeGem2"] == 0 && $_SESSION["consumeGem3"] == 0) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
          }
        }
        $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']] = "<img width='60px' height='60px' src='image/Solid_black.svg' alt='black'>";
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($row == $_SESSION['playerPoint_x'] && $col == $_SESSION['playerPoint_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint1_x'] && $col == $_SESSION['gemPoint1_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint2_x'] && $col == $_SESSION['gemPoint2_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint3_x'] && $col == $_SESSION['gemPoint3_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']];
              echo $_SESSION['board'][$row][$col];
            } else {
              echo $_SESSION['board'][$row][$col];
            }
          }
          echo "<br>";
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint1_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint1_y']) {
          if ($_SESSION["consumeGem1"] == 0) {
            score();
          }
          $_SESSION["consumeGem1"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint2_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint2_y']) {
          if ($_SESSION["consumeGem2"] == 0) {
            score();
          }
          $_SESSION["consumeGem2"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint3_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint3_y']) {
          if ($_SESSION["consumeGem3"] == 0) {
            score();
          }
          $_SESSION["consumeGem3"] = 1;
        }
      }
      function left() {
        if ($_SESSION["playerPoint_y"] > 0 && $_SESSION["playerPoint_y"] <= 9) {
          $_SESSION["playerPoint_y"] = $_SESSION["playerPoint_y"] - 1;
        }
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem3"] == 1 && $_SESSION["consumeGem1"] != 1 && $_SESSION["consumeGem2"] != 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
            if ($_SESSION["consumeGem1"] == 0 && $_SESSION["consumeGem2"] == 0 && $_SESSION["consumeGem3"] == 0) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/ruby.svg' alt='ruby'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/emerald.svg' alt='emerald'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/gem.svg' alt='gem'>";
            }
            if ($_SESSION["consumeGem1"] == 1 && $_SESSION["consumeGem2"] == 1 && $_SESSION["consumeGem3"] == 1) {
              $_SESSION['board'][$row][$col] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
              $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']] = "<img width='60px' height='60px' src='image/Solid_white.svg' alt='white'>";
            }
          }
        }
        $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']] = "<img width='60px' height='60px' src='image/Solid_black.svg' alt='black'>";
        for ($row = 0; $row < 10; $row ++) {
          for ($col = 0; $col < 10; $col ++) {
            if ($row == $_SESSION['playerPoint_x'] && $col == $_SESSION['playerPoint_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['player'][$_SESSION['playerPoint_x']][$_SESSION['playerPoint_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint1_x'] && $col == $_SESSION['gemPoint1_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint1_x']][$_SESSION['gemPoint1_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint2_x'] && $col == $_SESSION['gemPoint2_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint2_x']][$_SESSION['gemPoint2_y']];
              echo $_SESSION['board'][$row][$col];
            } else if ($row == $_SESSION['gemPoint3_x'] && $col == $_SESSION['gemPoint3_y']) {
              $_SESSION['board'][$row][$col] = $_SESSION['gem'][$_SESSION['gemPoint3_x']][$_SESSION['gemPoint3_y']];
              echo $_SESSION['board'][$row][$col];
            } else {
              echo $_SESSION['board'][$row][$col];
            }
          }
          echo "<br>";
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint1_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint1_y']) {
          if ($_SESSION["consumeGem1"] == 0) {
            score();
          }
          $_SESSION["consumeGem1"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint2_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint2_y']) {
          if ($_SESSION["consumeGem2"] == 0) {
            score();
          }
          $_SESSION["consumeGem2"] = 1;
        }
        if ($_SESSION['playerPoint_x'] == $_SESSION['gemPoint3_x'] && $_SESSION['playerPoint_y'] == $_SESSION['gemPoint3_y']) {
          if ($_SESSION["consumeGem3"] == 0) {
            score();
          }
          $_SESSION["consumeGem3"] = 1;
        }
      }
      ?>
    </div>
    <?php
    echo "<div id=\"controls\">";
    echo "<button><a href=\"?command=start\">start</a></button>";
    echo "<button><a href=\"?command=up\">up</a></button>";
    echo "<button><a href=\"?command=down\">down</a></button>";
    echo "<button><a href=\"?command=left\">left</a></button>";
    echo "<button><a href=\"?command=right\">right</a></button>";
    echo "<button><a href=\"?command=reset\">reset</a></button>";
    echo "</div>";
    function score() {
      $_SESSION["score"] = $_SESSION["score"] + 1;
    }
    ?>
    <h4>Score:
      <?php
      if (!isset($_SESSION['score'])) {
        $_SESSION["score"] = 0;
      }
      echo $_SESSION["score"];
      ?>
    </h4>
  </div>
</body>
<!-- <label>
<div>Icons made by <a href="https://www.flaticon.com/authors/freepik"
title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"
title="Flaticon">www.flaticon.com</a>
</div>
</label> -->
</html>
